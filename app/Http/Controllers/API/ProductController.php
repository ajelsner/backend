<?php

namespace App\Http\Controllers\API;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where(['status'=>'1'])->get();
        $response = array();
        $response['status'] = true;
        $response['message'] = 'Success';
        $response['data'] = $products;
        return response()->json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|max:191|unique:products,name',
            'image' => 'mimes:jpeg,jpg,png,gif|max:1024',
            'price' => 'required|numeric',
        );
        $data = $request->all();
        $validator = Validator::make($data, $rules);
        $response = array();
        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = $validator->messages()->first();
            return response()->json($response, 200);
        }
        Product::create($data);
        $response['status'] = true;
        $response['message'] = 'Record created.';
        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request  $request)
    {
        $rules = array(
            'id' => 'required|numeric|exists:products,id',
        );
        $data = $request->all();
        $validator = Validator::make($data, $rules);
        $response = array();
        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = $validator->messages()->first();
            return response()->json($response, 200);
        }
        $product = Product::find($request->id);
        $response['status'] = true;
        $response['message'] = 'Success';
        $response['data'] = $product;
        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request  $request)
    {
        $rules = array(
            'id' => 'required|numeric|exists:products,id',
        );
        $data = $request->all();
        $validator = Validator::make($data, $rules);
        $response = array();
        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = $validator->messages()->first();
            return response()->json($response, 200);
        }
        $product = Product::find($request->id);
        $response['status'] = true;
        $response['message'] = 'Success';
        $response['data'] = $product;
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = array(
            'id' => 'required|numeric|exists:products,id',
            'name' => 'sometimes|required|max:191|unique:products,name,'.$request->id,
            'image' => 'mimes:jpeg,jpg,png,gif|max:1024',
            'price' => 'sometimes|required|numeric',
            'status' => 'sometimes|required|in:0,1',
        );
        $data = $request->all();
        $validator = Validator::make($data, $rules);
        $response = array();
        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = $validator->messages()->first();
            return response()->json($response, 200);
        }
        $product = Product::find($request->id);
        $product->update($data);
        $response['status'] = true;
        $response['message'] = 'Record updated.';
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        $rules = array(
            'id' => 'required|numeric|exists:products,id',
        );
        $data = $request->all();
        $validator = Validator::make($data, $rules);
        $response = array();
        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = $validator->messages()->first();
            return response()->json($response, 200);
        }
        Product::destroy($request->id);
        $response['status'] = true;
        $response['message'] = 'Record deleted.';
        return response()->json($response, 200);
    }
}
