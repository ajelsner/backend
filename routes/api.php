<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
	Route::get('products','API\ProductController@index');
	Route::post('products/store','API\ProductController@store');
	Route::post('products/edit','API\ProductController@edit');
	Route::post('products/update','API\ProductController@update');
	Route::post('products/show','API\ProductController@show');
	Route::post('products/destroy','API\ProductController@destroy');
});

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
